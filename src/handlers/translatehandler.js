var utils = require('../utils/translateutils');

exports.morsetoText = function (req, res) {
     //.... --- .-.. .-   -- . .-.. ..
     let morse=req.body.text;
     let translatedCode='';
	 let codSplit=morse.split("   ");
     
	for (let i = 0; i < codSplit.length; i++) {			
			let letterMorse=codSplit[i].split(" ");
			for (let j = 0; j < letterMorse.length; j++) {				
			    let letter=letterMorse[j].replace(" ","").trim();                
			    translatedCode+=utils.getValueHash(letter);
			}			
			translatedCode+=" ";			
	}					
	return res.status(200).send({translated: translatedCode.trim() });
    
    
};

exports.textToMorse = function (req, res) {
    //HOLA MELI
    let text=req.body.text;
    let translatedCode='';
	let textSplit=text.split(" ");     
	for (let i = 0; i < textSplit.length; i++) {			
			let letterHuman=Array.from(textSplit[i]);            
			for (let j = 0; j < letterHuman.length; j++) {				                		
                if(j!=(letterHuman.length-1))   
			        translatedCode+=utils.getValueHash(letterHuman[j])+" ";
                else    
                    translatedCode+=utils.getValueHash(letterHuman[j]);
			}			
			translatedCode+="   ";			
	}					
	return res.status(200).send({translated: translatedCode.trim() });
    
    
};

