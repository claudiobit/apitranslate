
var chai = require('chai');
var server = require('../../server');
var expect = chai.expect;
var supertest=require('supertest')
var api=supertest('http://localhost:5000/translate');



describe('translate', () => {
    
  /*
  * Test the /2text route
  */
  describe('/2text Post', () => {
      it('it should show decode morse to human text', (done) => {
        
        api.post('/2text')
            .send({text:".... --- .-.. .-   -- . .-.. .."})
            .expect(200)
            .end((err, res) => {                 
                expect(res.body.translated).to.equal('HOLA MELI') ;              
                done();
            });
      });
  });


  /*
  * Test the /2morse route
  */
  describe('/2morse Post', () => {
      it('it should show human text translated to morse code', (done) => {
        
        api.post('/2morse')
            .send({text:"HOLA MELI"})
            .expect(200)
            .end((err, res) => {
               expect(res.body.translated).to.equal('.... --- .-.. .-   -- . .-.. ..') ;              
               done();
            });
      });
  });

});