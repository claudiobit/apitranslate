


var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),   
    cors=require("cors"),
    port = process.env.PORT || 5000;





app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors());



app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

var routes=require('./src/routes/routes');
app.use("/translate", routes);




app.listen(port, function() {
      console.log("Translator server running on http://localhost:"+port);
})

module.exports = app;

